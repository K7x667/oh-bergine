CREATE TABLE `users` (
	`id` serial AUTO_INCREMENT NOT NULL,
	`name` varchar(256) NOT NULL,
	`email` varchar(256) NOT NULL,
	`password` varchar(50) NOT NULL,
	`salt` varchar(100) NOT NULL,
	CONSTRAINT `users_id` PRIMARY KEY(`id`)
);
