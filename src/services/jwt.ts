import { sign } from 'hono/jwt';
import 'dotenv/config';

export function generateToken(userId: number) {
	const payload = {
		sub: userId,
		role: 'user',
		exp: Math.floor(Date.now() / 1000) + 60 * 60,
		iat: Math.floor(Date.now() / 1000),
		nbf: Math.floor(Date.now() / 1000),
	};

	return sign(payload, process.env.SECRET as string);
}
