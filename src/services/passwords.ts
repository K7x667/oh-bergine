import { scrypt, randomBytes, timingSafeEqual } from 'node:crypto';

export function hash(password: string): Promise<string> {
	return new Promise((resolve, reject) => {
		const salt = randomBytes(16).toString('hex');
		scrypt(password, salt, 64, (err, derivedKey) => {
			if (err) reject(err);
			resolve(salt + ':' + derivedKey.toString('hex'));
		});
	});
}

export function verify(password: string, hash: string): Promise<boolean> {
	return new Promise((resolve, reject) => {
		const [salt, key] = hash.split(':');
		const keyBuffer = Buffer.from(key, 'hex');
		scrypt(password, salt, 64, (err, derivedKey) => {
			if (err) reject(err);
			return resolve(timingSafeEqual(keyBuffer, derivedKey));
		});
	});
}
