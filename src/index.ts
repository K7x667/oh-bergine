import { serve } from '@hono/node-server';
import 'dotenv/config';
import { eq } from 'drizzle-orm';
import { Hono } from 'hono';
import { users } from './lib/models/schema';
import { db } from './services/db';
import { hash, verify as verifyHash } from './services/passwords';
import { generateToken } from './services/jwt';
import { zValidator } from '@hono/zod-validator';
import {
	loginSchema,
	registerSchema,
	safeReturnUser,
	updateUserSchema,
	welcomeNameSchema,
} from './lib/schemas';
import { jwt } from 'hono/jwt';
import * as z from 'zod';
import escapeHtml from 'escape-html';
import { cors } from 'hono/cors';
import { csrf } from 'hono/csrf';
import { logger } from 'hono/logger';
import { secureHeaders } from 'hono/secure-headers';

const app = new Hono();

app.use('*', logger());
app.use('*', secureHeaders());

app.use(
	'/api/*',
	jwt({
		secret: process.env.SECRET as string,
	})
);

app.use(
	'*',
	cors({
		origin: '*',
		allowHeaders: ['X-Custom-Header', 'Upgrade-Insecure-Requests'],
		allowMethods: ['POST', 'GET', 'OPTIONS', 'PUT', 'DELETE'],
		exposeHeaders: ['Content-Length'],
		maxAge: 600,
		credentials: true,
	})
);

app.use('*', csrf({ origin: '*' }));

app.post('/login', zValidator('json', loginSchema), async (c) => {
	const { username, password }: { username: string; password: string } = c.req.valid('json');

	const user = await db.query.users.findFirst({
		where: eq(users.username, username),
	});
	if (!user) {
		return c.text('Invalid username or password', 401);
	}

	const isPasswordValid = await verifyHash(password, user.password);
	if (!isPasswordValid) {
		return c.text('Invalid username or password', 401);
	}

	const token = await generateToken(user.id);
	return c.json({ user: safeReturnUser.parse(user), token });
});
app.post('/register', zValidator('json', registerSchema), async (c) => {
	const { email, password, username }: { email: string; password: string; username: string } =
		c.req.valid('json');

	const passwordHashed = await hash(password);
	await db.insert(users).values({
		email: email,
		password: passwordHashed,
		username: username,
	});

	const user = await db.query.users.findFirst({
		where: eq(users.email, email),
	});

	if (!user) {
		return c.text('There was an error in user registration', 500);
	}

	const token = await generateToken(user.id);

	return c.json({
		user: safeReturnUser.parse(user),
		token: token,
	});
});

// Route vulnérable au XSS
app.get('api/welcome', async (c) => {
	const name = welcomeNameSchema.parse(c.req.query('name'));

	return c.html(`<h1>Welcome, ${escapeHtml(name)}!</h1>`);
});

app.post('api/update-profile', zValidator('json', updateUserSchema), async (c) => {
	const { email, password, username } = c.req.valid('json');
	const payload = c.get('jwtPayload');

	const updateData: { email?: string; password?: string; username?: string } = {
		email: email ?? undefined,
		password: password ? await hash(password) : undefined,
		username: username ?? undefined,
	};

	await db.update(users).set(updateData).where(eq(users.id, payload.sub));

	return c.text('Profile updated successfully');
});

app.onError((err, c) => {
	if (err instanceof z.ZodError) {
		return c.text('Invalid data sent', 400);
	}

	console.log(err);

	return c.text('There was an internal server error, please contact administrator', 500);
});

const port = 3000;
console.log(`Server is running on port ${port}`);

serve({
	fetch: app.fetch,
	port,
});
